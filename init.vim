" ################################################
" General
" ################################################

set background=light

set termguicolors
set nocompatible              " required
filetype on
syntax on

set encoding=utf-8

set number
let mapleader = ","
set nowrap                  " Dont wrap files
set gdefault                " Search replace globally on a line by default
set incsearch               " Show search matches while typing
set mouse=a                 " Allow use of mouse
set clipboard=unnamed       " Use regular clipboard
set hidden                  " Hide buffers instead of closing them
set tw=0
set nohlsearch
set foldlevel=99

" ################################################
" Actions
" ################################################

" Exit insert mode with jk
inoremap jk <esc>
inoremap <esc> <nop>

" Map curson move keys to switch between split windows
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
" Terrible hack to get move keys working in tmux
if has('nvim')
	nmap <BS> <C-W><C-h>
endif

" Fold
nnoremap <space> za

" Save, quit and savequit leaders
nnoremap <leader>w :w<cr>
nnoremap <leader>q :q<cr>

" New line from insert mode
imap <C-o> <esc>o

" Autoformat complete file
nnoremap <leader>i G=gg

" Close all buffers except current
nnoremap <leader>ca :w <bar> %bd <bar> e#<cr>

" Add shortcut for opening .vimrc
nnoremap <leader>ev :e $MYVIMRC<cr>
" Save and load vimrc
nnoremap <leader>sv :w<cr>:source $MYVIMRC<cr>

" Delete all trailing whitespace
nnoremap <leader>dtw :%s/\s\+$//e

"Move down remap
nnoremap <C-g> 30jzz

" Buffer list
nnoremap <leader>b :buffers<cr>:b
" Go to last buffer
nnoremap <leader><tab> :b#<cr>
" Next and previous buffer
nnoremap <Tab> :bnext<cr>
nnoremap <S-Tab> :bprevious<cr>
" Close buffer
nnoremap <leader>x :bd<cr>

" Highlight search toggle
nnoremap <leader>h :set hls!<cr>

" Add space after function opening
inoremap {<cr> {<cr>}<c-o>O

" Run
autocmd FileType python nnoremap <buffer> <leader>p :e<cr>:!python %<cr>
autocmd FileType javascript nnoremap <leader>p :e<cr>:!node %<cr>

" Compile and run C
map <F8> : !gcc % && ./a.out <CR>

" Run make and run program for c files with make file
nnoremap <leader>m :w<CR> :!clear; make<CR> :!./%<<CR>

" Fixes bug 'editing must be done in place' for crontab
autocmd filetype crontab setlocal nobackup nowritebackup

" ### Plugin specific ###

" Add shortcut for opening snippet editor
nnoremap <leader>u :UltiSnipsEdit<cr>

" NERDTree toggle
nnoremap <leader>t :NERDTreeToggle<cr>
" NERDTree exclude .pyc files
let NERDTreeIgnore=['\.pyc$', '\~$'] "ignore files in NERDTree

" Taglist
nnoremap <leader>l :TlistToggle<cr>
let Tlist_Use_Right_Window = 1
let Tlist_WinWidth = 60

" Show buffers on airline when one tab open
let g:airline#extensions#tabline#enabled = 1

" ################################################
" File Specific
" ################################################

au BufRead,BufNewFile *.pde set filetype=processing

" txt
au BufNewFile,BufRead *.txt
    \ set wrap |
    \ set textwidth=79

" markdown
au BufNewFile,BufRead *.md
    \ set wrap |
    \ set textwidth=79

" PEP8 Python
au BufNewFile,BufRead *.py
    \ set tabstop=4 |
    \ set softtabstop=4 |
    \ set shiftwidth=4 |
    \ set textwidth=79 |
    \ set expandtab |
    \ set autoindent |
    \ set fileformat=unix |

" C
au BufNewFile,BufRead *.c
    \ set foldmethod=indent

" js
au BufNewFile,BufRead *.js
    \ set tabstop=2 |
    \ set softtabstop=2 |
    \ set shiftwidth=2 |
    \ set textwidth=99 |
    \ set expandtab |
    \ set autoindent |
    \ set fileformat=unix |
    \ set foldmethod=syntax

" HTML tab setting
au BufNewFile,BufRead *.html
    \ set tabstop=2 |
    \ set shiftwidth=2

" Autocomment python files
:autocmd FileType python nnoremap <buffer> <leader>c I#<esc>
" Autocomment processing files
:autocmd FileType processing nnoremap <buffer> <leader>c I//<esc>
" Add self to start of word
:autocmd FileType python nnoremap <buffer> <leader>s biself.<esc>
" Disable python preview from autocomplete
autocmd FileType python setlocal completeopt-=preview


" ################################################
" PLUGINS
" ################################################

call plug#begin('~/.local/share/nvim/plugged')

" Essentials
Plug 'scrooloose/nerdtree'
Plug 'kien/ctrlp.vim'
Plug 'ervandew/supertab'
Plug 'vim-scripts/taglist.vim'
" Plug 'altercation/vim-colors-solarized'
Plug 'frankier/neovim-colors-solarized-truecolor-only'

" Various
Plug 'kassio/neoterm'
Plug 'yegappan/mru'

" Markdown
Plug 'plasticboy/vim-markdown'

" Syntax checking
" Plug 'scrooloose/syntastic'
Plug 'w0rp/ale'

" Insert mods
Plug 'gregsexton/MatchTag'
Plug 'Townk/vim-autoclose'
Plug 'godlygeek/tabular'

" Python
Plug 'vim-scripts/indentpython.vim'
Plug 'tmhedberg/SimpylFold'

" Javascript
Plug 'pangloss/vim-javascript'
Plug 'mxw/vim-jsx'

" Typescript
Plug 'leafgarland/typescript-vim'

" Airline
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" Snippets
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'

" Autocomplete
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'davidhalter/jedi-vim'
Plug 'zchee/deoplete-jedi'
Plug 'zchee/deoplete-clang'
Plug 'carlitux/deoplete-ternjs', { 'for': ['javascript', 'javascript.jsx'] }

call plug#end()

" Syntastic
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

" Linters and checkers
let g:ale_linters = {
\   'python': ['flake8'],
\   'javascript': ['eslint'],
\}

" Use deoplete
let g:deoplete#enable_at_startup = 1

" deoplete-clang
let g:deoplete#sources#clang#libclang_path = '/Library/Developer/CommandLineTools/usr/lib/libclang.dylib'
let g:deoplete#sources#clang#clang_header = '/usr/local/Cellar/llvm/4.0.0_1/lib/clang'

" UltiSnips
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<tab-s>"
let g:UltiSnipsEditSplit="vertical"
let g:UltiSnipsSnippetsDir = "~/.vim/mySnippets/"
let g:UltiSnipsSnippetDirectories = ["UltiSnips", "mySnippets"]

" Make tab cycle from top to bottom
inoremap <expr><TAB> pumvisible() ? "\<C-n>" : "\<TAB>"

" Set viewtype of complete sources
set completeopt=longest,menuone,preview
set completeopt-=preview

" JSX
let g:jsx_ext_required = 0

" deoplete sources
let g:deoplete#sources = {}
let g:deoplete#sources._ = ['file', 'ultisnips']
let g:deoplete#sources.html = ['file', 'ultisnips']
let g:deoplete#sources.py = ['file', 'ultisnips', 'jedi']
let g:deoplete#sources.js = ['file', 'ultisnips', 'ternjs']
let g:deoplete#sources.c = ['file', 'clang']

" Colorscheme has to be in the end
colorscheme solarized
